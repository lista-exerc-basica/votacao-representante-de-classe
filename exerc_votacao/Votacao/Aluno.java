

public class Aluno
{
    private String nome, voto;
    
    public Aluno() {
           
    }
    
    public Aluno(String nome, String voto) {
        this.nome = nome;
        this.voto = voto;
    }
    
    public String getNome() {
        return this.nome;   
    }
    
    public void setNome(String nome) {
        this.nome = nome;   
    }
    
    public String getVoto() {
        return this.voto;   
    }
    
    public void setVoto(String voto) {
        this.voto = voto;   
    }
}
