import java.util.ArrayList;

public class Turma
{
    
    private ArrayList<Aluno> alunos;
    private String vencedor;
    
        public Turma() {
        alunos = new ArrayList<>();   
    }
    
    public int qtdAlunos() {
        int totalAlunos = 0;
        
        for(Aluno n : alunos) {
            totalAlunos++;
        }
        
        return totalAlunos;
    }
    
    public int confereVotos() {
        
        int count = 0;
        int maior = 0; 
        
        for(int j = 0; j < alunos.size(); j++){
            count = 0;
            String voto = getAlunos().get(j).getVoto();
                for(int i = 0; i < alunos.size(); i++) {
                                        
                    if(voto.equals(getAlunos().get(i).getVoto())){
                         count++;   
                    }
                    
                }
                
            if(count > maior) {
                maior = count;
                setVencedor(voto);
            }
        }
        
        return maior;   
    }
    
    public double porcentagemVotos() {
        return confereVotos() / (alunos.size() / 100.0) ;
    }
    
    public void addAluno(Aluno aluno){
        this.alunos.add(aluno);
    }
        
    public String toString() {
        
        String votou = "";
        String nomeRepetido = "";
        if(alunos.size() > 1){
            for(int i = 0; i < alunos.size();i++) {
                String nome = getAlunos().get(i).getNome();
                    for(int j = 1; j < alunos.size(); j++){
                        nomeRepetido = getAlunos().get(j).getNome();
                            if(nome.equals(nomeRepetido)){
                               votou = "O aluno que votou mais de uma vez terá os votos sobresalentes removidos!!"; 
                               removeAluno(getAlunos().get(i));
                            }

                    }
                    
                    /*if(nomeRepetido.equals(nome)){
                        votou = "O aluno não pode votar e seu segundo voto será removido!!!"; 
                            removeAluno(getAlunos().get(i));
                    }else {
                        votou = "O voto computado";   
                        }*/
            }
        }
        return votou;  
    }
    
    public void removeAluno(Aluno alunos){
        this.alunos.remove(alunos);   
    }
    
    public String getVencedor() {
        return this.vencedor;  
    }
    
    public void setVencedor(String vencedor) {
        this.vencedor = vencedor;   
    }
    
    public ArrayList<Aluno> getAlunos() {
        return alunos;
    }
}

