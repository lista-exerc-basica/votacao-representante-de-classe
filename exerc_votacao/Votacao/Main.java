import java.util.Scanner;


public class Main
{
    public static void main(String[] argrs) {
     
        
        Scanner le = new Scanner(System.in);
        
        Turma t = new Turma();
        
        System.out.println("#####Sistema votação representante de turma#####");
        
        while(true) {
            Aluno n = new Aluno();
            
            System.out.println("Informe o seu nome para votar : ");
            n.setNome(le.next());
            
            System.out.println("qual seu voto?");
            n.setVoto(le.next());
            
            t.addAluno(n);
    
            System.out.println("Deseja incluir mais alunos? (S)im/(N)ão");
            if(le.next().equalsIgnoreCase("N")){
                break;
            }
            
            le.nextLine();
            
        }
        System.out.println(t.toString());
        System.out.println("O vencedor da eleição tem o total de  " + t.confereVotos() +" votos (" + t.porcentagemVotos() + "%), ");
        System.out.println("Nome do ganhador é: " + t.getVencedor());
    }
}
